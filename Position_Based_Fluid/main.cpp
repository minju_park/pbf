#include <stdio.h>
#include <stdlib.h>
#include <gl\GL.H>
#include <gl\GLU.H>
#include <gl\glut.h>
#include <gl\GLAUX.H>
#include "PBF.h"

int mode = -1;

PBF pbf;

void Initialize()
{
	pbf.Initialize();
}

void Render()
{
	pbf.Render();

	glutSwapBuffers();
}

void Idle()
{
	if (mode == 1)
	{
		pbf.Update();
	}

	glutPostRedisplay();
}

void Keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 32:
		mode = mode * -1;
		break;
	case 'q':
		exit(0);
		break;
	case 27:
		exit(0);
		break;
	default:
		break;
	}
	glutPostRedisplay();
}

void Mouse(int mouse_event, int state, int x, int y)
{
	pbf.myMouse(mouse_event, state, x, y);

	glutPostRedisplay();
}


void Motion(int x, int y)
{
	pbf.myMotion(x, y);

	glutPostRedisplay();
}

void Reshape(int w, int h)
{
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60.0, w / h, 1.0, 300.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

int main(int argc, char* argv[])
{
	Initialize();
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
	glutInitWindowPosition(800, 0);
	glutInitWindowSize(800, 800);
	glutCreateWindow("Position Based Fluid");
	glutDisplayFunc(Render);
	glutReshapeFunc(Reshape);
	glutKeyboardFunc(Keyboard);
	glutIdleFunc(Idle);
	glutMouseFunc(Mouse);
	glutMotionFunc(Motion);

	glutMainLoop();

	return 0;
}