#include "PBF.h"
#include "FileManager.h"

#include <gl\GL.H>
#include <gl\GLU.H>
#include <gl\glut.h>
#include <gl\GLAUX.H>


FileManager cap;

PBF::PBF()
{
	np = 0;
	d0 = 1000.0;
	phi = 3.14;
	q = 0.5;
	m = 250.0;
	t = 0.016;
	h = 1.0;
	ep = 0.5;		
	c = 0.01;

	bx1 = 1.0;	bx2 = 15.0;
	by1 = 1.0;	by2 = 15.0;
	bz1 = 1.0;	bz2 = 15.0;

	X = 50;
	Y = 50;
	Z = 50;
}


PBF::~PBF()
{
}

float PBF::Kernel(Vector r, float h)
{
	float W, nr = r.norm();

	if (nr >= 0.0 && nr <= h)
		W = (315 * (h*h - nr*nr)*(h*h - nr*nr)*(h*h - nr*nr)) / (64 * phi*pow(h, 9));
	else
		W = 0.0;

	return W;
}

Vector PBF::G_Kernel(Vector r, float h)
{
	Vector W;
	float nr = r.norm();
	r.normalize();
	if (nr > 0.0 && nr <= h)
		W = r*(-45.0f * (h - nr)*(h - nr) / (phi*pow(h, 6)));
	else
		W.equal(0.0f);

	return W;
}

void PBF::Initialize()
{
	for (float i = 1.1; i < 8.0; i += q)
	{
		for (float j = 1.1; j < 8.0; j += q)
		{
			for (float k = 1.1; k < 8.0; k += q)
			{
					Particle p0;

					p0.position.x = i;
					p0.position.y = j;
					p0.position.z = k;

					p0.predict_position.equal(0.0);
					p0.velocity.equal(0.0);
					p0.position_variation.equal(0.0);
					p0.lambda = 0.0;
					p0.density = d0;
					p0.particle_num = np;

					p0.vorticity_force.y = 0.0;

					P.push_back(p0);
					np++;
			}
		}
	}
	printf("입자 개수 : %d\n", np);
}

void PBF::Find_Neighbor()
{
	for (int i = 0; i < np; i++)
	{
		int hx = P[i].predict_position.x;
		int hy = P[i].predict_position.y;
		int hz = P[i].predict_position.z;
	 
		for (int a = -1; a <= 1; a++)
		{
			for (int b = -1; b <= 1; b++)
			{
				for (int c = -1; c <= 1; c++)
				{
					for (int j = 0; j < hash[(hx + a) + (hy + b)*X + (hz + c)*X*Y].pP.size(); j++)
					{
						if (hx + a < bx1 || hx + a>bx2 || hy + b < by1 || hy + b > by2 || hz + c < bz1 || hz + c>bz2)
							continue;
					
						int l = hash[(hx + a) + (hy + b)*X + (hz + c)*X*Y].pP[j]->particle_num;

						if (l == i) continue;

						P[i].push_neighbor(&P[l]);
					}
				}
			}
		} 
	}
}

void PBF::Clear_Neighbor()
{
	for (int i = 0; i < np; i++)
	{
		P[i].clear_neighbor();
	}
}

void PBF::Density()
{
	for (int i = 0; i < np; i++)
	{
		float dens = 0.0;
	 
		for (int j = 0; j < P[i].N.size(); j++)
		{
			if (i == P[i].N[j]->particle_num)
				continue;

			Vector o;
			o = P[i].predict_position - P[i].N[j]->predict_position;
			dens += m*Kernel(o, h);
		}

		P[i].density = dens;
	}
}

void PBF::Lambda()
{
	for (int i = 0; i < np; i++)
	{
		Vector gdc1, gdc2; // gradient of distance constraints

		float sum = 0.0;
		float lam = 0.0;

		for (int j = 0; j < P[i].N.size(); j++)
		{
			if (i == P[i].N[j]->particle_num)
				continue;

			Vector o = P[i].predict_position - P[i].N[j]->predict_position;
			
			gdc1 = gdc1+(G_Kernel(o, h) / d0);
		}

		sum = gdc1.norm()*gdc1.norm();	

		for (int j = 0; j < P[i].N.size(); j++)
		{
			if (i == P[i].N[j]->particle_num)
				continue;

			Vector o = P[i].predict_position - P[i].N[j]->predict_position;

			gdc2 = G_Kernel(o, h)*(-1.0)/d0;
			
			sum += gdc2.norm()*gdc2.norm();
		}
		float constraint = (P[i].density / d0) - 1.0;

		P[i].lambda = -0.25*(constraint) / (sum + ep);
	}
}

void PBF::Position_Variation()
{
	for (int i = 0; i < np; i++)
	{
		Vector dp;
		dp.equal(0.0);

		for (int j = 0; j < P[i].N.size(); j++)
		{
			if (i == P[i].N[j]->particle_num)
				continue;

			Vector o;
			o = P[i].predict_position - P[i].N[j]->predict_position;

			Vector w(0.15*h, 0.15*h, 0.0);
		 	float s = -0.1*pow((Kernel(o, h) / Kernel(w, h)), 4);
			 
			dp = dp + ((G_Kernel(o, h)*(P[i].lambda + P[i].N[j]->lambda + s)) / d0);
		}
		P[i].position_variation = dp;

	}
}

void PBF::Position_Update()
{
	for (int i = 0; i < np; i++)
	{
		P[i].predict_position = P[i].predict_position + P[i].position_variation;
	}
}

void PBF::Vorticity()
{
	for (int i = 0; i < np; i++)
	{
		Vector vor;
		vor.equal(0.0);

		for (int j = 0; j < P[i].N.size(); j++)
		{
			if (i == P[i].N[j]->particle_num)
				continue;

			Vector v1, v2;
			v1 = P[i].N[j]->velocity - P[i].velocity;
			v2 = P[i].predict_position - P[i].N[j]->predict_position;
			
			vor = vor + v1.cross(G_Kernel(v2, h));
		}
		P[i].vorticity = vor;
	}
}

void PBF::Vorticity_Force()
{
	for (int i = 0; i < np; i++)
	{
		Vector eta;
		Vector N;

		for (int j = 0; j < P[i].N.size(); j++)
		{
			if (i == P[i].N[j]->particle_num)
				continue;

			Vector o;
			o = P[i].predict_position - P[i].N[j]->predict_position;

			float u = P[i].N[j]->vorticity.norm();

			eta = eta + G_Kernel(o, h)*u;
		}
		N = eta / eta.norm();
	
		P[i].vorticity_force = N.cross(P[i].vorticity);
	}
}

void PBF::Viscosity()
{
	for (int i = 0; i < np; i++)
	{
		Vector sum;

		for (int j = 0; j < P[i].N.size(); j++)
		{
			Vector v1, v2;
			v1 = P[i].N[j]->velocity - P[i].velocity;
			v2 = P[i].predict_position - P[i].N[j]->predict_position;

			sum = sum + v1*Kernel(v2, h);
		}
		P[i].velocity = P[i].velocity + c*sum;
	}
}

void PBF::Update()
{
	Find_Neighbor();

	Density();

	for (int i = 0; i < 3; i++)
	{
		Lambda();
	 	Position_Variation();
		Position_Update();
	}

	for (int i = 0; i < np; i++)
	{
		P[i].velocity = (P[i].predict_position - P[i].position) / t;
		P[i].position = P[i].predict_position;
	}

	Vorticity();
	Vorticity_Force();
	Viscosity();

	Distance_com();
}

void PBF::myMouse(int mouse_event, int state, int x, int y)
{
	Mouse_Coord[0] = x;
	Mouse_Coord[1] = y;

	switch (mouse_event)
	{
	case GLUT_LEFT_BUTTON:
		Mouse_Event[0] = ((GLUT_DOWN == state) ? 1 : 0);
		break;
	case GLUT_MIDDLE_BUTTON:
		Mouse_Event[1] = ((GLUT_DOWN == state) ? 1 : 0);
		break;
	case GLUT_RIGHT_BUTTON:
		Mouse_Event[2] = ((GLUT_DOWN == state) ? 1 : 0);
		break;
	default:
		break;
	}
}

void PBF::myMotion(int x, int y)
{
	int diffx = x - Mouse_Coord[0];
	int diffy = y - Mouse_Coord[1];

	Mouse_Coord[0] = x;
	Mouse_Coord[1] = y;

	if (Mouse_Event[0])
	{
		rotation.x += (float) 0.1 * diffy;
		rotation.y += (float) 0.1 * diffx;
	}
	else if (Mouse_Event[1])
	{
		translation.x += (float) 0.1 * diffx;
		translation.y -= (float) 0.1 * diffy;
	}
	else if (Mouse_Event[2])
	{
		Zoom += (float) 0.1 * diffy + (float) 0.1 * diffx;
	}
}

void PBF::Render()
{
	glEnable(GL_DEPTH_TEST);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();

	gluLookAt(25.0, 15.0, 15.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);

	glPushMatrix();
	glTranslatef(-Zoom, -Zoom, -Zoom*5.0/3.0);
	glTranslatef(translation.x, translation.y, 0.0);
	glRotatef(rotation.x, 1.0, 0.0, 0.0);
	glRotatef(rotation.y, 0.0, 1.0, 0.0);
	glTranslatef(-bx2 / 2.0, -by2 / 2.0, -bz2 / 2.0);
	glPointSize(5);
	glBegin(GL_POINTS);
	for (int i = 0; i < np; i++)
	{
		{
			// 표면 색 다르게
			if (P[i].distance_com.norm() <= 0.65)
			{
				glColor3f(1.0, 1.0, 1.0);
				glVertex3f(P[i].position.x, P[i].position.y, P[i].position.z);
			}
			else
			{
				glColor3f(0.4, 0.55, 0.85);
				glVertex3f(P[i].position.x, P[i].position.y, P[i].position.z);
			}
		}


		// for basic PBF
		Vector cor = P[i].position;
		cor = cor / cor.norm();
		glColor3f(cor.x, cor.y, cor.z);
		glVertex3f(P[i].position.x, P[i].position.y, P[i].position.z);
	}
	glEnd();
	glLineWidth(1.0);
	glColor3f(1.0f, 1.0f, 1.0f);
	glBegin(GL_LINE_LOOP);
	glVertex3f(bx1, by1, bz1);
	glVertex3f(bx2, by1, bz1);
	glVertex3f(bx2, by1, bz2);
	glVertex3f(bx1, by1, bz2);
	glEnd();
	glBegin(GL_LINE_LOOP);
	glVertex3f(bx1, by2, bz1);
	glVertex3f(bx2, by2, bz1);
	glVertex3f(bx2, by2, bz2);
	glVertex3f(bx1, by2, bz2);
	glEnd();
	glBegin(GL_LINES);
	glVertex3f(bx1, by1, bz1);
	glVertex3f(bx1, by2, bz1);
	glVertex3f(bx2, by1, bz1);
	glVertex3f(bx2, by2, bz1);
	glVertex3f(bx2, by1, bz2);
	glVertex3f(bx2, by2, bz2);
	glVertex3f(bx1, by1, bz2);
	glVertex3f(bx1, by2, bz2);
	glEnd();
	glPopMatrix();

	//cap.ScreenCapture();


}

// surface 추출
void PBF::Distance_com()
{
	for (int i = 0; i < np; i++)
	{
		Vector distance; distance.equal(0.0f);
		Vector sum_neigbor; sum_neigbor.equal(0.0f);
		float sum_mass; sum_mass = 0.0f;

		for (int j = 0; j < P[i].N.size(); j++)
		{
			sum_mass += 1;
			sum_neigbor = sum_neigbor + P[i].N[j]->predict_position;
		}

		distance = P[i].position - (sum_neigbor / sum_mass);


		P[i].distance_com = distance;
	}
}