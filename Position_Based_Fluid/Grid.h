#pragma once
#include <vector>
#include "Particle.h"

class Grid
{
public:
	Grid();
	~Grid();

	std::vector<Particle*> pP;

	void push_Particle(Particle *p);
	void clear_Particle();
};

