#include <iostream>
#include "FileManager.h"

using namespace std;


FileManager::FileManager(void)
{
	m_bitmapIdx = 1;
	m_width		= 800;
	m_height	= 800;
}


FileManager::~FileManager(void)
{

}


void FileManager::ScreenCapture()
{
	char filename[255];

	int dir = _mkdir("Scene");

	sprintf_s(filename, 255, "Scene//%d.bmp", m_bitmapIdx);

	BITMAPFILEHEADER bf;
	BITMAPINFOHEADER bi;

	unsigned char *image = (unsigned char*)malloc(sizeof(unsigned char)*m_width*m_height*3);
	FILE *file;
	fopen_s(&file, filename, "wb");
	if( image!=NULL )
	{
		if( file!=NULL ) 
		{
			glReadPixels( 0, 0, m_width, m_height, GL_RGB, GL_UNSIGNED_BYTE, image );

			memset( &bf, 0, sizeof( bf ) );
			memset( &bi, 0, sizeof( bi ) );

			bf.bfType = 'MB';
			bf.bfSize = sizeof(bf)+sizeof(bi)+m_width*m_height*3;
			bf.bfOffBits = sizeof(bf)+sizeof(bi);
			bi.biSize = sizeof(bi);
			bi.biWidth = m_width;
			bi.biHeight = m_height;
			bi.biPlanes = 1;
			bi.biBitCount = 24;
			bi.biSizeImage = m_width*m_height*3;

			fwrite( &bf, sizeof(bf), 1, file );
			fwrite( &bi, sizeof(bi), 1, file );
			fwrite( image, sizeof(unsigned char), m_height*m_width*3, file );

			fclose( file );
		}
		free( image );
	}

	m_bitmapIdx++;
}

