#pragma once
#include "Particle.h"
#include "Grid.h"
#include <vector>
#include "Vector.h"

class PBF
{
public :
	int		np;		//number of particles
	float	d0;		//rest density
	float	phi;
	float	q;		//distance between two particles
	float	m;		//mass
	float	t;		//time step
	float	h;		//for Kernel
	float	ep;		//relaxtion parameter
	float	c;		//constant for Visctosity
	float	k;		//gas constant
	float	bx1, bx2;
	float	by1, by2;
	float	bz1, bz2;	//boundary coordinate

	int					Mouse_Coord[2];	// previous mouse coordinates
	unsigned char		Mouse_Event[3];	// mouse event handler
	float				Zoom;				// view zoom
	Vector				rotation;
	Vector				translation;

	int X;
	int Y;
	int Z;		//size of HASH

	std::vector<Particle> P;
	Grid hash[125000];

public:
	PBF();
	~PBF();

	float Kernel(Vector r, float h);
	Vector G_Kernel(Vector r, float h);
	void Initialize();
	void Find_Neighbor();
	void Clear_Neighbor();
	void Density();
	void Lambda();
	void Position_Variation();
	void Position_Update();
	void Vorticity();
	void Vorticity_Force();
	void Viscosity();
	void Update();
	void Render();
	void myMouse(int mouse_event, int state, int x, int y);
	void myMotion(int x, int y);

	void Distance_com();
};

