#pragma once
#include "Vector.h"
#include <vector>
class Particle
{
public:
	Particle();
	~Particle();

	std::vector<Particle*> N;

	Vector position;				//initial position
	Vector predict_position;		//position
	Vector velocity;				//velocity
	Vector position_variation;		//total position update
	Vector vorticity;				//vorticity
	Vector vorticity_force;			//vorticity force
	int particle_num;				//particle number
	float lambda;	
	float density;					//density
	Vector distance_com;

	void push_neighbor(Particle *n);
	void clear_neighbor();
};

